<?php

namespace frontend\modules\user\controllers;

use Yii;
use yii\web\Controller;
use frontend\models\User;

use Faker\Factory;

class ProfileController extends Controller
{
    public function actionView($nickname)
    {
        $currentUser = Yii::$app->user->identity;

        return $this->render('view', [
            'user' => $this->findUser($nickname),
            'currentUser' => $currentUser,
        ]);
    }

    private function findUser($nickname)
    {
        if ($user = User::find()->where(['nickname' => $nickname])->orWhere(['id' => $nickname])->one()){
            return $user;
        }
        throw new NotFoundHttpException();
    }

    public function actionSubscribe($id)
    {
        if (Yii::$app->user->isGuest){
            return $this->redirect(['/user/default/login']);
        }

        $currentUser = Yii::$app->user->identity; //пользователь

        $user = $this->findUserById($id);//пользователь на которого нужно подписаться

        if( $currentUser->id != $user->id) {
                $currentUser->followUser($user);
           }
//         echo '<pre>';
//         print_r($currentUser);
//         print_r($user);
//         echo '<pre>';
//         die;
        return $this->redirect(['/user/profile/view', 'nickname' =>$user->getNickname()]);
    }

    public function actionUnsubscribe($id)
    {
        if(Yii::$app->user->isGuest){
            return $this->redirect(['/user/default/login']);
        }

        /* @var $currentUser User */

        $currentUser=Yii::$app->user->identity;
        $user = $this->findUser($id);

        if( $currentUser->id != $user->id) {
               $currentUser->unfollowUser($user);
         };

         return $this->redirect(['/user/profile/view', 'nickname' => $user->getNickname()]);
    }

    private function findUserById($id)
    {
        if ($user = User::findOne($id))
        {
            return $user;
        }
        throw new NotFoundHttpException();
    }

//     /**
//          * @throws \yii\base\Exception
//          */
//         public function actionGenerate()
//         {
//             $faker = Factory::create();
//
//             for ($i = 0; $i < 1000; $i++) {
//                 $user = new User([
//                     'username' => $faker->name,
//                     'email' => $faker->email,
//                     'about' => $faker->text,
//                     'nickname' => $faker->regexify('[A-Za-z0-9_]{5,15}'),
//                     'auth_key' => Yii::$app->security->generateRandomString(),
//                     'password_hash' => Yii::$app->security->generateRandomString(),
//                 ]);
//                 $user->save(false);
//             }
//         }
//

}